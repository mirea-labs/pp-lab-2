﻿#include <iostream>
#include <fstream>
#include <omp.h>
#include <time.h>
#include <chrono>

using namespace std;

int main();
void finalize(int** a, int** b, int** c, int n);
void random_matrix(int** a, int n);
void matrix_output(int** a, int n);
void reset_matrix(int** matrix, int size);
void execute_test_case(int** a, int** b, int** c, int n, ofstream& output_row, ofstream& output_column, ofstream& output_delta);
void multiply_matrix_row_major(int** a, int** b, int** c, int n);
void multiply_matrix_column_major(int** a, int** b, int** c, int n);

int main() {
	srand(time(0));

	ofstream output_row, output_column, output_delta;
	output_row.open("output_row.csv");
	output_column.open("output_column.csv");
	output_delta.open("output_delta.csv");

	for (int n = 500; n <= 1000; n += 100) {
		cout << "Matrix " << n << "x" << n << ": ";
		int** a = new int* [n], ** b = new int* [n], ** c = new int* [n];
		for (int i = 0; i < n; i++) {
			a[i] = new int[n];
			b[i] = new int[n];
			c[i] = new int[n];
		}
		random_matrix(a, n);
		random_matrix(b, n);

		execute_test_case(a, b, c, n, output_row, output_column, output_delta);

		finalize(a, b, c, n);

		output_row << endl;
		output_column << endl;
		output_delta << endl;
		cout << endl;
	}

	output_row.close();
	output_column.close();

	cout << "Calculations are saved to the output_*.csv files." << endl;

	return 0;
}

void finalize(int** a, int** b, int** c, int n) {
	for (int i = 0; i < n; i++)
	{
		delete[] a[i];
		delete[] b[i];
		delete[] c[i];
	}

	delete[] a;
	delete[] b;
	delete[] c;
}

void execute_test_case(int** a, int** b, int** c, int n, ofstream& output_row, ofstream& output_column, ofstream& output_delta) {
	for (int threads = 1; threads <= 64; threads *= 2) {
		cout << threads;

		omp_set_num_threads(threads);

		reset_matrix(c, n);
		auto begin = chrono::high_resolution_clock::now();
		multiply_matrix_row_major(a, b, c, n);
		auto end = chrono::high_resolution_clock::now();
		long long time_row_wise = chrono::duration_cast<chrono::nanoseconds>(end - begin).count();
		output_row << time_row_wise / 1000000000.0 << ',';
		cout << "r";

		reset_matrix(c, n);
		begin = chrono::high_resolution_clock::now();
		multiply_matrix_column_major(a, b, c, n);
		end = chrono::high_resolution_clock::now();
		long long time_column_wise = chrono::duration_cast<chrono::nanoseconds>(end - begin).count();
		output_column << time_column_wise / 1000000000.0 << ',';
		cout << "c ";

		output_delta << (time_row_wise - time_column_wise) / 1000000000.0 << ',';
	}
}

void random_matrix(int** a, int n) {
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			a[i][j] = rand() % 10;
		}
	}
}

void reset_matrix(int** matrix, int size) {
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			matrix[i][j] = 0;
		}
	}
}

void multiply_matrix_row_major(int** a, int** b, int** c, int n) {
	int row = 0, column = 0, k = 0;
#pragma omp parallel for shared(c) private(row,column,k)
	for (row = 0; row < n; row++)
		for (column = 0; column < n; column++)
			for (k = 0; k < n; k++)
				c[row][column] += a[row][k] * b[k][column];
}

void multiply_matrix_column_major(int** a, int** b, int** c, int n) {
	int k = 0, column = 0, row = 0;
#pragma omp parallel for shared(c) private(row,column,k)
	for (row = 0; row < n; row++)
		for (column = 0; column < n; column++)
			for (k = 0; k < n; k++)
				c[k][column] += a[k][row] * b[row][column];
}

void matrix_output(int** a, int n) {
	cout << endl;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			printf("%5d ", a[i][j]);
		}
		cout << endl;
	}
	cout << endl;
}